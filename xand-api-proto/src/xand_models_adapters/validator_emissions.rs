use core::convert::TryFrom;

use crate::{
    error::XandApiProtoErrs,
    proto_models::{
        ValidatorEmissionProgress as XMValidatorEmissionProgress,
        ValidatorEmissionRate as XMValidatorEmissionRate,
    },
    ValidatorEmissionProgress, ValidatorEmissionRate,
};

impl From<ValidatorEmissionRate> for XMValidatorEmissionRate {
    fn from(proto_validator_emission_rate: ValidatorEmissionRate) -> XMValidatorEmissionRate {
        XMValidatorEmissionRate {
            minor_units_per_emission: proto_validator_emission_rate.minor_units_per_emission,
            block_quota: proto_validator_emission_rate.block_quota,
        }
    }
}

impl TryFrom<ValidatorEmissionProgress> for XMValidatorEmissionProgress {
    type Error = XandApiProtoErrs;

    fn try_from(
        proto_validator_emission_progress: ValidatorEmissionProgress,
    ) -> Result<XMValidatorEmissionProgress, Self::Error> {
        let rate = proto_validator_emission_progress
            .effective_emission_rate
            .ok_or_else(|| XandApiProtoErrs::ConversionError {
                reason: "No effective emission rate found during conversion".to_string(),
            })?;
        Ok(XMValidatorEmissionProgress {
            effective_emission_rate: rate.into(),
            blocks_completed_progress: proto_validator_emission_progress.blocks_completed_progress,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        proto_models::{
            ValidatorEmissionProgress as XMValidatorEmissionProgress,
            ValidatorEmissionRate as XMValidatorEmissionRate,
        },
        ValidatorEmissionProgress, ValidatorEmissionRate,
    };

    #[allow(non_snake_case)]
    #[test]
    fn from__validator_emission_rate() {
        let validator_emission_rate = ValidatorEmissionRate {
            minor_units_per_emission: 0,
            block_quota: 1,
        };

        let xm_validator_emission_rate =
            XMValidatorEmissionRate::from(validator_emission_rate.clone());

        assert_eq!(
            validator_emission_rate.minor_units_per_emission,
            xm_validator_emission_rate.minor_units_per_emission
        );
        assert_eq!(
            validator_emission_rate.block_quota,
            xm_validator_emission_rate.block_quota
        );
    }

    #[allow(non_snake_case)]
    #[test]
    fn try_from__validator_emission_progress() {
        let validator_emission_progress = ValidatorEmissionProgress {
            effective_emission_rate: Some(ValidatorEmissionRate {
                minor_units_per_emission: 0,
                block_quota: 1,
            }),
            blocks_completed_progress: 0,
        };

        let xm_validator_emission_progress =
            XMValidatorEmissionProgress::try_from(validator_emission_progress.clone()).unwrap();

        assert_eq!(
            validator_emission_progress
                .effective_emission_rate
                .clone()
                .unwrap()
                .minor_units_per_emission,
            xm_validator_emission_progress
                .effective_emission_rate
                .minor_units_per_emission
        );
        assert_eq!(
            validator_emission_progress
                .effective_emission_rate
                .clone()
                .unwrap()
                .block_quota,
            xm_validator_emission_progress
                .effective_emission_rate
                .block_quota
        );
        assert_eq!(
            validator_emission_progress.blocks_completed_progress,
            xm_validator_emission_progress.blocks_completed_progress
        )
    }

    #[allow(non_snake_case)]
    #[test]
    fn try_from__validator_emission_progress_errors() {
        let validator_emission_progress = ValidatorEmissionProgress {
            effective_emission_rate: None,
            blocks_completed_progress: 0,
        };

        let xm_validator_emission_progress_error =
            XMValidatorEmissionProgress::try_from(validator_emission_progress).unwrap_err();

        assert_eq!(
            xm_validator_emission_progress_error,
            XandApiProtoErrs::ConversionError {
                reason: "No effective emission rate found during conversion".to_string()
            }
        )
    }
}
