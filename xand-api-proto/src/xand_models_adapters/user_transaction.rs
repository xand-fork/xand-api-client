use super::*;
use error::XandApiProtoErrs;
use proto_models::{
    errors::EncryptionError, AllowlistCidrBlock as XMAllowlistCidrBlock, BankAccountId,
    BankAccountInfo, CashConfirmation as XMCashConfirmation, CorrelationId,
    CreateCancellation as XMCreateCancellation, PendingCreateRequest as XMPendingCreateRequest,
    PendingRedeemRequest as XMPendingRedeemRequest, RedeemCancellation as XMRedeemCancellation,
    RedeemFulfillment as XMRedeemFulfillment, RegisterSessionKeys as XMRegisterSessionKeys,
    RemoveAllowlistCidrBlock as XMRemoveAllowlistCidrBlock, Send as XMSend,
    SetPendingCreateRequestExpire as XMSetPendingCreateRequestExpire,
    WithdrawFromNetwork as XMWithdrawFromNetwork, XandTransaction,
};
use std::str::FromStr;

impl TryFrom<XandTransaction> for user_txn::Operation {
    type Error = XandApiProtoErrs;
    fn try_from(txn: XandTransaction) -> Result<Self, Self::Error> {
        Ok(match txn {
            XandTransaction::Send(t) => user_txn::Operation::Send(t.into()),
            XandTransaction::CreateRequest(c) => user_txn::Operation::CreateRequest(c.into()),
            XandTransaction::RedeemRequest(r) => user_txn::Operation::RedeemRequest(r.into()),
            XandTransaction::CashConfirmation(m) => user_txn::Operation::CashConfirmation(m.into()),
            XandTransaction::RedeemFulfillment(s) => {
                user_txn::Operation::RedeemFulfillment(s.into())
            }
            XandTransaction::CreateCancellation(inner) => {
                user_txn::Operation::CreateCancellation(inner.into())
            }
            XandTransaction::RedeemCancellation(r) => {
                user_txn::Operation::RedeemCancellation(r.into())
            }
            XandTransaction::AllowlistCidrBlock(inner) => {
                user_txn::Operation::AllowlistCidrBlock(inner.into())
            }
            XandTransaction::RemoveAllowlistCidrBlock(inner) => {
                user_txn::Operation::RemoveCidrBlock(inner.into())
            }
            XandTransaction::RegisterSessionKeys(inner) => {
                user_txn::Operation::RegisterSessionKeys(inner.into())
            }
            XandTransaction::SetPendingCreateRequestExpire(inner) => {
                user_txn::Operation::SetPendingCreateRequestExpire(inner.into())
            }
            XandTransaction::WithdrawFromNetwork(inner) => {
                user_txn::Operation::WithdrawFromNetwork(inner.into())
            }
            XandTransaction::SetLimitedAgent(_)
            | XandTransaction::SetValidatorEmissionRate(_)
            | XandTransaction::RegisterMember(_)
            | XandTransaction::SetTrust(_)
            | XandTransaction::SetMemberEncryptionKey(_)
            | XandTransaction::SetTrustEncryptionKey(_)
            | XandTransaction::AddAuthorityKey(_)
            | XandTransaction::RemoveAuthorityKey(_)
            | XandTransaction::RootAllowlistCidrBlock(_)
            | XandTransaction::RootRemoveAllowlistCidrBlock(_)
            | XandTransaction::RemoveMember(_)
            | XandTransaction::ExitMember(_)
            | XandTransaction::RuntimeUpgrade(_) => {
                return Err(XandApiProtoErrs::ConversionError {
                    reason: format!("Transaction {:?} is an administrative transaction", txn),
                })
            }
            XandTransaction::SubmitProposal(_) | XandTransaction::VoteProposal(_) => {
                return Err(XandApiProtoErrs::ConversionError {
                    reason: format!("Transaction {:?} is not a user-issuable transaction", txn),
                })
            }
        })
    }
}

impl From<XMSend> for Send {
    fn from(t: XMSend) -> Self {
        Self {
            amount: t.amount_in_minor_unit,
            to: t.destination_account.to_string(),
        }
    }
}
impl TryFrom<Send> for XMSend {
    type Error = XandApiProtoErrs;
    fn try_from(x: Send) -> Result<Self, Self::Error> {
        Ok(Self {
            amount_in_minor_unit: x.amount,
            destination_account: x.to.parse()?,
        })
    }
}

impl From<XMPendingCreateRequest> for CreateRequest {
    fn from(c: XMPendingCreateRequest) -> Self {
        CreateRequest {
            amount: c.amount_in_minor_unit,
            correlation_id: c.correlation_id.to_string(),
            bank_account: Some(c.account.into()),
        }
    }
}

impl TryFrom<CreateRequest> for XMPendingCreateRequest {
    type Error = XandApiProtoErrs;
    fn try_from(x: CreateRequest) -> Result<Self, Self::Error> {
        Ok(XMPendingCreateRequest {
            correlation_id: CorrelationId::from_str(&x.correlation_id)?,
            account: x
                .bank_account
                .ok_or_else(|| XandApiProtoErrs::ConversionError {
                    reason: "Create request missing account field".to_string(),
                })?
                .try_into()?,
            amount_in_minor_unit: x.amount,
            completing_transaction: None,
        })
    }
}

impl From<XMPendingCreateRequest> for Create {
    fn from(mut x: XMPendingCreateRequest) -> Self {
        // `completing_transaction` is not used when converting to `CreateRequest` so it's ok to
        // `take` it.
        let completing_transaction = x.completing_transaction.take().map(Into::into);
        Self {
            request: Some(x.into()),
            completing_transaction,
        }
    }
}

impl TryFrom<Create> for XMPendingCreateRequest {
    type Error = XandApiProtoErrs;

    fn try_from(x: Create) -> Result<Self, Self::Error> {
        let completing_transaction = x.completing_transaction.map(|t| t.try_into()).transpose()?;
        let request = x.request.ok_or_else(|| XandApiProtoErrs::ConversionError {
            reason: "Submitted create missing request field".into(),
        })?;
        Ok(Self {
            completing_transaction,
            ..Self::try_from(request)?
        })
    }
}

impl TryFrom<PendingCreateRequest> for XMPendingCreateRequest {
    type Error = XandApiProtoErrs;

    fn try_from(x: PendingCreateRequest) -> Result<Self, Self::Error> {
        x.request
            .ok_or_else(|| XandApiProtoErrs::ConversionError {
                reason: "Pending create request missing request field".into(),
            })?
            .try_into()
    }
}

impl From<XMPendingRedeemRequest> for RedeemRequest {
    fn from(r: XMPendingRedeemRequest) -> Self {
        Self {
            amount: r.amount_in_minor_unit,
            correlation_id: r.correlation_id.to_string(),
            bank_account: Some(r.account.into()),
        }
    }
}
impl TryFrom<RedeemRequest> for XMPendingRedeemRequest {
    type Error = XandApiProtoErrs;
    fn try_from(x: RedeemRequest) -> Result<Self, Self::Error> {
        Ok(Self {
            correlation_id: CorrelationId::from_str(&x.correlation_id)?,
            account: x
                .bank_account
                .ok_or_else(|| XandApiProtoErrs::ConversionError {
                    reason: "Redeem request missing account field".to_string(),
                })?
                .try_into()?,
            amount_in_minor_unit: x.amount,
            completing_transaction: None,
        })
    }
}

impl TryFrom<BankAccount> for BankAccountInfo {
    type Error = XandApiProtoErrs;
    fn try_from(b: BankAccount) -> Result<Self, Self::Error> {
        let account = b.account.ok_or_else(|| XandApiProtoErrs::ConversionError {
            reason: "BankAccount missing account field".into(),
        })?;
        match account {
            xand_api::bank_account::Account::UnencryptedBankAccount(b) => {
                Ok(BankAccountInfo::Unencrypted(BankAccountId {
                    account_number: b.account_number,
                    routing_number: b.routing_number,
                }))
            }
            xand_api::bank_account::Account::EncryptedBankAccount(_) => {
                Ok(BankAccountInfo::Encrypted(EncryptionError::KeyNotFound))
            }
        }
    }
}

impl From<BankAccountInfo> for BankAccount {
    fn from(b: BankAccountInfo) -> Self {
        match b {
            BankAccountInfo::Unencrypted(account) => BankAccount {
                account: Some(xand_api::bank_account::Account::UnencryptedBankAccount(
                    xand_api::UnencryptedBankAccount {
                        routing_number: account.routing_number,
                        account_number: account.account_number,
                    },
                )),
            },
            BankAccountInfo::Encrypted(error) => BankAccount {
                account: Some(xand_api::bank_account::Account::EncryptedBankAccount(
                    xand_api::EncryptedBankAccount {
                        encryption_error: Some(error.into()),
                    },
                )),
            },
        }
    }
}

impl From<EncryptionError> for xand_api::EncryptionError {
    fn from(e: EncryptionError) -> Self {
        match e {
            EncryptionError::KeyNotFound => Self {
                error: Some(xand_api::encryption_error::Error::KeyNotFound(
                    xand_api::KeyNotFound {},
                )),
            },
            EncryptionError::MessageMalformed => Self {
                error: Some(xand_api::encryption_error::Error::MessageMalformed(
                    xand_api::MessageMalformed {},
                )),
            },
        }
    }
}

impl From<XMPendingRedeemRequest> for Redeem {
    fn from(mut x: XMPendingRedeemRequest) -> Self {
        // `completing_transaction` is not used when converting to `RedeemRequest` so it's ok to
        // `take` it.
        let completing_transaction = x.completing_transaction.take().map(Into::into);
        Self {
            request: Some(x.into()),
            completing_transaction,
        }
    }
}

impl TryFrom<Redeem> for XMPendingRedeemRequest {
    type Error = XandApiProtoErrs;

    fn try_from(x: Redeem) -> Result<Self, Self::Error> {
        let completing_transaction = x.completing_transaction.map(|t| t.try_into()).transpose()?;
        let request = x.request.ok_or_else(|| XandApiProtoErrs::ConversionError {
            reason: "Redeem request missing request field".into(),
        })?;
        Ok(Self {
            completing_transaction,
            ..XMPendingRedeemRequest::try_from(request)?
        })
    }
}

impl TryFrom<PendingRedeemRequest> for XMPendingRedeemRequest {
    type Error = XandApiProtoErrs;

    fn try_from(x: PendingRedeemRequest) -> Result<Self, Self::Error> {
        x.request
            .ok_or_else(|| XandApiProtoErrs::ConversionError {
                reason: "Pending redeem request missing request field".into(),
            })?
            .try_into()
    }
}

impl From<XMCashConfirmation> for CashConfirmation {
    fn from(m: XMCashConfirmation) -> Self {
        Self {
            correlation_id: m.correlation_id.to_string(),
        }
    }
}
impl TryFrom<CashConfirmation> for XMCashConfirmation {
    type Error = XandApiProtoErrs;
    fn try_from(x: CashConfirmation) -> Result<Self, Self::Error> {
        Ok(Self {
            correlation_id: CorrelationId::from_str(&x.correlation_id)?,
        })
    }
}

impl From<XMRedeemFulfillment> for RedeemFulfillment {
    fn from(s: XMRedeemFulfillment) -> Self {
        Self {
            correlation_id: s.correlation_id.to_string(),
        }
    }
}
impl TryFrom<RedeemFulfillment> for XMRedeemFulfillment {
    type Error = XandApiProtoErrs;
    fn try_from(x: RedeemFulfillment) -> Result<Self, Self::Error> {
        Ok(Self {
            correlation_id: CorrelationId::from_str(&x.correlation_id)?,
        })
    }
}

impl From<XMCreateCancellation> for CreateCancellation {
    fn from(c: XMCreateCancellation) -> Self {
        Self {
            correlation_id: c.correlation_id.to_string(),
            reason: c.reason.to_string(),
        }
    }
}
impl TryFrom<CreateCancellation> for XMCreateCancellation {
    type Error = XandApiProtoErrs;
    fn try_from(x: CreateCancellation) -> Result<Self, Self::Error> {
        Ok(Self {
            correlation_id: CorrelationId::from_str(&x.correlation_id)?,
            reason: x
                .reason
                .parse()
                .map_err(|_| XandApiProtoErrs::ConversionError {
                    reason: "Invalid create cancellation reason".into(),
                })?,
        })
    }
}

impl From<XMRedeemCancellation> for RedeemCancellation {
    fn from(c: XMRedeemCancellation) -> Self {
        Self {
            correlation_id: c.correlation_id.to_string(),
            reason: c.reason.to_string(),
        }
    }
}
impl TryFrom<RedeemCancellation> for XMRedeemCancellation {
    type Error = XandApiProtoErrs;
    fn try_from(x: RedeemCancellation) -> Result<Self, Self::Error> {
        Ok(Self {
            correlation_id: CorrelationId::from_str(&x.correlation_id)?,
            reason: x
                .reason
                .parse()
                .map_err(|_| XandApiProtoErrs::ConversionError {
                    reason: "Invalid redeem cancellation reason".into(),
                })?,
        })
    }
}

impl From<XMAllowlistCidrBlock> for AllowlistCidrBlock {
    fn from(w: XMAllowlistCidrBlock) -> Self {
        Self {
            cidr_block: w.cidr_block.to_string(),
        }
    }
}
impl TryFrom<AllowlistCidrBlock> for XMAllowlistCidrBlock {
    type Error = XandApiProtoErrs;
    fn try_from(x: AllowlistCidrBlock) -> Result<Self, Self::Error> {
        Ok(Self {
            cidr_block: x.cidr_block.parse()?,
        })
    }
}

impl From<XMRemoveAllowlistCidrBlock> for RemoveAllowlistCidrBlock {
    fn from(w: XMRemoveAllowlistCidrBlock) -> Self {
        Self {
            cidr_block: w.cidr_block.to_string(),
        }
    }
}
impl TryFrom<RemoveAllowlistCidrBlock> for XMRemoveAllowlistCidrBlock {
    type Error = XandApiProtoErrs;
    fn try_from(x: RemoveAllowlistCidrBlock) -> Result<Self, Self::Error> {
        Ok(Self {
            cidr_block: x.cidr_block.parse()?,
        })
    }
}

impl From<XMSetPendingCreateRequestExpire> for SetPendingCreateRequestExpire {
    fn from(s: XMSetPendingCreateRequestExpire) -> Self {
        Self {
            expire_in_milliseconds: s.expire_in_milliseconds,
        }
    }
}
impl From<SetPendingCreateRequestExpire> for XMSetPendingCreateRequestExpire {
    fn from(x: SetPendingCreateRequestExpire) -> Self {
        Self {
            expire_in_milliseconds: x.expire_in_milliseconds,
        }
    }
}

impl From<XMRegisterSessionKeys> for RegisterSessionKeys {
    fn from(x: XMRegisterSessionKeys) -> Self {
        Self {
            block_production_pubkey: x.block_production_pubkey.to_string(),
            block_finalization_pubkey: x.block_finalization_pubkey.to_string(),
        }
    }
}

impl TryFrom<RegisterSessionKeys> for XMRegisterSessionKeys {
    type Error = XandApiProtoErrs;

    fn try_from(x: RegisterSessionKeys) -> Result<Self, Self::Error> {
        Ok(Self {
            block_production_pubkey: x.block_finalization_pubkey.parse()?,
            block_finalization_pubkey: x.block_finalization_pubkey.parse()?,
        })
    }
}

impl From<WithdrawFromNetwork> for XMWithdrawFromNetwork {
    fn from(_: WithdrawFromNetwork) -> Self {
        Self {}
    }
}

impl From<XMWithdrawFromNetwork> for WithdrawFromNetwork {
    fn from(_: XMWithdrawFromNetwork) -> Self {
        Self {}
    }
}
