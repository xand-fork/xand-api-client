use super::*;

use crate::error::XandApiProtoErrs;
use crate::proto_models::Blockstamp as XandModelsBlockStamp;
use crate::Blockstamp;

impl TryFrom<Blockstamp> for XandModelsBlockStamp {
    type Error = XandApiProtoErrs;
    fn try_from(proto_blockstamp: Blockstamp) -> Result<Self, Self::Error> {
        let timestamp_ms = match proto_blockstamp.timestamp {
            Some(t) => t.unix_time_millis,
            None => {
                return Err(XandApiProtoErrs::ConversionError {
                    reason: "No timestamp found during conversion".to_string(),
                })
            }
        };

        Ok(XandModelsBlockStamp {
            block_number: proto_blockstamp.block_number,
            unix_timestamp_ms: timestamp_ms,
            is_stale: proto_blockstamp.is_stale,
        })
    }
}
