mod admin_transaction;
mod blockstamp;
mod health_check;
mod network_voting_user_transaction;
mod transaction;
mod user_transaction;
mod validator_emissions;

pub use admin_transaction::*;
pub use network_voting_user_transaction::*;
pub use transaction::*;
pub use user_transaction::*;
pub use validator_emissions::*;

use super::*;
use crate::error::XandApiProtoErrs;
use crate::proto_models::XandTransaction;

impl From<XandTransaction> for any_transaction::Operation {
    fn from(txn: XandTransaction) -> Self {
        match txn {
            XandTransaction::RegisterSessionKeys(r) => {
                any_transaction::Operation::RegisterSessionKeys(r.into())
            }
            XandTransaction::Send(t) => any_transaction::Operation::Send(t.into()),
            XandTransaction::CreateRequest(c) => {
                any_transaction::Operation::CreateRequest(c.into())
            }
            XandTransaction::RedeemRequest(r) => {
                any_transaction::Operation::RedeemRequest(r.into())
            }
            XandTransaction::CashConfirmation(m) => {
                any_transaction::Operation::CashConfirmation(m.into())
            }
            XandTransaction::RedeemFulfillment(s) => {
                any_transaction::Operation::RedeemFulfillment(s.into())
            }
            XandTransaction::CreateCancellation(c) => {
                any_transaction::Operation::CreateCancellation(c.into())
            }
            XandTransaction::RedeemCancellation(c) => {
                any_transaction::Operation::RedeemCancellation(c.into())
            }
            XandTransaction::AllowlistCidrBlock(w) => {
                any_transaction::Operation::AllowlistCidrBlock(w.into())
            }
            XandTransaction::RemoveAllowlistCidrBlock(w) => {
                any_transaction::Operation::RemoveCidrBlock(w.into())
            }
            XandTransaction::RegisterMember(r) => {
                any_transaction::Operation::RegisterAccountAsMember(r.into())
            }
            XandTransaction::RemoveMember(r) => any_transaction::Operation::RemoveMember(r.into()),
            XandTransaction::ExitMember(r) => any_transaction::Operation::ExitMember(r.into()),
            XandTransaction::SetTrust(t) => any_transaction::Operation::SetTrust(t.into()),
            XandTransaction::SetMemberEncryptionKey(k) => {
                any_transaction::Operation::SetMemberEncryptionKey(k.into())
            }
            XandTransaction::SetTrustEncryptionKey(k) => {
                any_transaction::Operation::SetTrustEncryptionKey(k.into())
            }
            XandTransaction::AddAuthorityKey(a) => {
                any_transaction::Operation::AddAuthorityKey(a.into())
            }
            XandTransaction::RemoveAuthorityKey(a) => {
                any_transaction::Operation::RemoveAuthorityKey(a.into())
            }
            XandTransaction::SetLimitedAgent(la) => {
                any_transaction::Operation::SetLimitedAgent(la.into())
            }
            XandTransaction::SetValidatorEmissionRate(a) => {
                any_transaction::Operation::SetValidatorEmissionRate(a.into())
            }
            XandTransaction::RootAllowlistCidrBlock(r) => {
                any_transaction::Operation::RootAllowlistCidrBlock(r.into())
            }
            XandTransaction::RootRemoveAllowlistCidrBlock(r) => {
                any_transaction::Operation::RootRemoveAllowlistCidrBlock(r.into())
            }
            XandTransaction::SetPendingCreateRequestExpire(p) => {
                any_transaction::Operation::SetPendingCreateRequestExpire(p.into())
            }
            XandTransaction::SubmitProposal(p) => {
                any_transaction::Operation::SubmitProposal(p.into())
            }
            XandTransaction::VoteProposal(v) => any_transaction::Operation::VoteProposal(v.into()),
            XandTransaction::RuntimeUpgrade(r) => {
                any_transaction::Operation::RuntimeUpgrade(r.try_into().expect("A RuntimeUpgrade.xand_hash should never error while being serialized into bytes"))
            }
            XandTransaction::WithdrawFromNetwork(w) => {
                any_transaction::Operation::WithdrawFromNetwork(w.into())
            }
        }
    }
}

impl TryFrom<any_transaction::Operation> for XandTransaction {
    type Error = XandApiProtoErrs;
    fn try_from(txn: any_transaction::Operation) -> Result<Self, Self::Error> {
        Ok(match txn {
            any_transaction::Operation::RegisterSessionKeys(r) => {
                XandTransaction::RegisterSessionKeys(r.try_into()?)
            }
            any_transaction::Operation::Send(t) => XandTransaction::Send(t.try_into()?),
            any_transaction::Operation::CreateRequest(c) => {
                XandTransaction::CreateRequest(c.try_into()?)
            }
            any_transaction::Operation::RedeemRequest(r) => {
                XandTransaction::RedeemRequest(r.try_into()?)
            }
            any_transaction::Operation::CashConfirmation(m) => {
                XandTransaction::CashConfirmation(m.try_into()?)
            }
            any_transaction::Operation::RedeemFulfillment(s) => {
                XandTransaction::RedeemFulfillment(s.try_into()?)
            }
            any_transaction::Operation::CreateCancellation(c) => {
                XandTransaction::CreateCancellation(c.try_into()?)
            }
            any_transaction::Operation::RedeemCancellation(c) => {
                XandTransaction::RedeemCancellation(c.try_into()?)
            }
            any_transaction::Operation::AllowlistCidrBlock(w) => {
                XandTransaction::AllowlistCidrBlock(w.try_into()?)
            }
            any_transaction::Operation::RemoveCidrBlock(w) => {
                XandTransaction::RemoveAllowlistCidrBlock(w.try_into()?)
            }
            any_transaction::Operation::RegisterAccountAsMember(r) => {
                XandTransaction::RegisterMember(r.try_into()?)
            }
            any_transaction::Operation::RemoveMember(r) => {
                XandTransaction::RemoveMember(r.try_into()?)
            }
            any_transaction::Operation::ExitMember(r) => XandTransaction::ExitMember(r.try_into()?),
            any_transaction::Operation::SetTrust(t) => XandTransaction::SetTrust(t.try_into()?),
            any_transaction::Operation::SetLimitedAgent(a) => {
                XandTransaction::SetLimitedAgent(a.try_into()?)
            }
            any_transaction::Operation::SetValidatorEmissionRate(a) => {
                XandTransaction::SetValidatorEmissionRate(a.into())
            }
            any_transaction::Operation::SetMemberEncryptionKey(k) => {
                XandTransaction::SetMemberEncryptionKey(k.try_into()?)
            }
            any_transaction::Operation::SetTrustEncryptionKey(k) => {
                XandTransaction::SetTrustEncryptionKey(k.try_into()?)
            }
            any_transaction::Operation::AddAuthorityKey(a) => {
                XandTransaction::AddAuthorityKey(a.try_into()?)
            }
            any_transaction::Operation::RemoveAuthorityKey(a) => {
                XandTransaction::RemoveAuthorityKey(a.try_into()?)
            }
            any_transaction::Operation::RootAllowlistCidrBlock(r) => {
                XandTransaction::RootAllowlistCidrBlock(r.try_into()?)
            }
            any_transaction::Operation::RootRemoveAllowlistCidrBlock(r) => {
                XandTransaction::RootRemoveAllowlistCidrBlock(r.try_into()?)
            }
            any_transaction::Operation::SetPendingCreateRequestExpire(p) => {
                XandTransaction::SetPendingCreateRequestExpire(p.into())
            }
            any_transaction::Operation::SubmitProposal(p) => {
                XandTransaction::SubmitProposal(p.try_into()?)
            }
            any_transaction::Operation::VoteProposal(v) => XandTransaction::VoteProposal(v.into()),
            any_transaction::Operation::RuntimeUpgrade(r) => {
                XandTransaction::RuntimeUpgrade(r.try_into()?)
            }
            any_transaction::Operation::WithdrawFromNetwork(w) => {
                XandTransaction::WithdrawFromNetwork(w.into())
            }
        })
    }
}
