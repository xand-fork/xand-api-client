use crate::proto_models::{
    public_key::PublicKeyParseError, CidrBlockParseErr, CorrelationIdError, TransactionIdError,
};
use xand_address::AddressError;

#[derive(Debug, snafu::Snafu, Clone, Eq, PartialEq, serde::Serialize)]
pub enum XandApiProtoErrs {
    ConversionError { reason: String },
}

impl From<Box<bincode::ErrorKind>> for XandApiProtoErrs {
    fn from(e: Box<bincode::ErrorKind>) -> Self {
        XandApiProtoErrs::ConversionError {
            reason: format!("Couldn't parse serialized hash: {:?}", e),
        }
    }
}

impl From<AddressError> for XandApiProtoErrs {
    fn from(e: AddressError) -> Self {
        XandApiProtoErrs::ConversionError {
            reason: format!("Couldn't parse address: {:?}", e),
        }
    }
}

impl From<CorrelationIdError> for XandApiProtoErrs {
    fn from(e: CorrelationIdError) -> Self {
        XandApiProtoErrs::ConversionError {
            reason: format!("Couldn't parse correlation id: {:?}", e),
        }
    }
}

impl From<TransactionIdError> for XandApiProtoErrs {
    fn from(e: TransactionIdError) -> Self {
        XandApiProtoErrs::ConversionError {
            reason: format!("Couldn't parse transaction id: {:?}", e),
        }
    }
}

impl From<CidrBlockParseErr> for XandApiProtoErrs {
    fn from(e: CidrBlockParseErr) -> Self {
        XandApiProtoErrs::ConversionError {
            reason: format!("Couldn't parse cidr block: {:?}", e),
        }
    }
}

impl From<PublicKeyParseError> for XandApiProtoErrs {
    fn from(_e: PublicKeyParseError) -> Self {
        XandApiProtoErrs::ConversionError {
            reason: "Could not parse public key".to_string(),
        }
    }
}
