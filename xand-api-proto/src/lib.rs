#![forbid(unsafe_code)]

#[allow(clippy::all)] // Prevent generated code from being checked by clippy
mod xand_api {
    // The string specified here must match the proto package name
    include!(concat!(env!("OUT_DIR"), "/xand_api.rs"));
}

pub mod error;

pub mod proto_models;
pub mod xand_models_adapters;

pub use xand_api::{
    administrative_transaction as admin_txn, user_transaction as user_txn, SubmitProposal,
    VoteProposal, *,
};
pub use xand_models_adapters::*;

use std::convert::{TryFrom, TryInto};
