//! Serialization of a byte sequence to an hexadecimal string
//!
//! This module can be used as the serde-with attribute to serialize a sequence of bytes to an
//! hexadecimal string prefixed by `0x`. If the prefix is not desired, the `hex-serde` crate can
//! be used.

use hex::FromHex;
use serde::{Deserializer, Serializer};
use std::fmt::{self, Display};
use std::marker::PhantomData;

/// Serializes a sequence of bytes to an hexadecimal string prefixed by "0x".
pub fn serialize<T, S>(x: &T, serializer: S) -> Result<S::Ok, S::Error>
where
    T: AsRef<[u8]>,
    S: Serializer,
{
    serializer.serialize_str(&format!("0x{}", hex::encode(x)))
}

/// Deserializes a sequence of bytes from an hexadecimal string optionally prefixed by "0x".
pub fn deserialize<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
    T: FromHex,
    T::Error: Display,
    D: Deserializer<'de>,
{
    struct Visitor<T>(PhantomData<fn() -> T>);

    impl<'de, T> serde::de::Visitor<'de> for Visitor<T>
    where
        T: FromHex,
        T::Error: Display,
    {
        type Value = T;

        fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, r#"an hex string optionally prefixed by "0x""#)
        }

        fn visit_str<E>(self, s: &str) -> Result<T, E>
        where
            E: serde::de::Error,
        {
            let s = s.strip_prefix("0x").unwrap_or(s);
            T::from_hex(s).map_err(serde::de::Error::custom)
        }
    }

    deserializer.deserialize_str(Visitor(PhantomData))
}
