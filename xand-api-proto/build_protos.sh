#!/usr/bin/env bash
set -e

mkdir -p ../rest-gateway/src/xand_api

echo "$GOPATH"

protoc -I/usr/local/include -I. \
       --grpc-gateway_out=logtostderr=true:../rest-gateway/src/xand_api \
       xand-api.proto

protoc -I/usr/local/include -I. \
       --go_out=plugins=grpc:../rest-gateway/src/xand_api \
       xand-api.proto

protoc -I/usr/local/include -I. \
       --swagger_out=logtostderr=true,json_names_for_fields=true,fqn_for_swagger_name=true:../rest-gateway/ \
       xand-api.proto