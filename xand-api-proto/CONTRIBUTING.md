[[_TOC_]]

# Contributing

## Changelog
When making changes to this repo, please make updates to the [CHANGELOG](CHANGELOG.md).

## Style guide

* Prefer explicit imports in `use` statements, over wildcard "*"

## Curators' Section

This section is to document ongoing design work or technical debt. If you have questions about any of these items please contact one of the Curators identified in this crate's [CODEOWNERS](CODEOWNERS) file.

---
* **Overall xand-api-proto**
    * Consider namespacing the generated client produced by `xand-api/proto` to something more specific like `xand_api_client`
    * Implement more specific encryption error handling on client-level (beyond key not found or malformed message) to provide more guidance to API consumer

* **xand-api-proto xand-models**
    * Consider a nested hierarchy for XandTransaction, where subsets of types are appropriately grouped for special handling - eg Administrative Transactions
    * Consider separating xand_api server and client tests; stub out the components not under test (for server tests, one would stub out the Client and its protobuf replies; for client tests, one would stub out the Transaction Cache and its results). This also allows us to decouple the tests and depend less on `xand_api_proto` in `xand_api`

* **break dependency on thermite's `xand-address` crate**
    * The goal is to stop publishing crates from thermite (it will exclusively publish docker images).
    * Unless/until `xand-address` is extracted to a separate repo, `xand-api-proto` will need to stop depending on it. In the immediate term we've left it as-is, but it will need to be addressed. See the TODO in `Cargo.toml`.
  
* **serde_hex.rs**
   * Duplicating this entire file between xand-api-proto::proto_models and xand_models does not make sense. Possibly extract into its own crate.
