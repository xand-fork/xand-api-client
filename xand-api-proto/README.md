<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [xand-api-proto](#xand-api-proto)
- [Generated Files](#generated-files)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# xand-api-proto

This crate contains the protobuf / gRPC definitions for the `xand-api`. It
will autogenerate rust code from those definitions when built. 

This crate also houses its own wire and business types inside its `xand_api_proto::proto_models` module, which is shared with the `xand-api-client`. 

To see what's generated and exposed as part of the public API run `cargo doc --open`.

# Generated Files

[`./build.rs`](./build.rs) uses [`tonic-build`](https://crates.io/crates/tonic-build) to generate Rust modules at compile-time from protobuf files.

- [`./xand-api.proto`](./xand-api.proto) yields `${OUT_DIR}/xand_api.rs`

In turn, [`./src/lib.rs`](./lib.rs) creates modules with the contents of those files. Consequently, the Rust types corresponding to the protobuf definitions are made available by this crate.
