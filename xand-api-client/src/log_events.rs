#[logging_event]
pub enum LoggingEvent {
    ReconnectingClientError(String),
}
