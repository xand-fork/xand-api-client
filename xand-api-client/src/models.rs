//! The types in here represent chain-agnostic versions of the data one might fetch or send with
//! the xand-api client. They are not the actual wire types, but are more usable representations.
//!
//! These may seem to fill the same purpose as `xand_models`, but there is an important
//! distinction. Some things, like petri, need to view a chain-agnostic model of the world, but
//! cannot depend on the xand-api-client. Hence, the only things that should be put in here are
//! things that consumers of the client need, but not anything the `xand-api` server itself needs.

use serde::{Deserialize, Serialize};
use xand_api_proto::proto_models::{TransactionId, TransactionStatus};

#[derive(Debug, Default, Clone)]
pub struct Paginated<T> {
    pub data: T,
    pub total: u32,
}

impl<T> From<Vec<T>> for Paginated<Vec<T>> {
    fn from(v: Vec<T>) -> Self {
        #[allow(clippy::cast_possible_truncation)]
        let total = v.len() as u32;

        Self { total, data: v }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Paging {
    /// Number of entries per page.
    pub page_size: u32,
    /// Page number starting from zero.
    pub page_number: u32,
}

impl Paging {
    /// Returns paging details suitable to get all items on a single page.
    #[must_use]
    pub fn all() -> Paging {
        Paging {
            page_size: u32::max_value(),
            page_number: 0,
        }
    }
}

impl Default for Paging {
    fn default() -> Paging {
        Paging {
            page_size: xand_api_proto::proto_models::DEFAULT_PAGE_SIZE,
            page_number: 0,
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub struct TransactionUpdate {
    pub id: TransactionId,
    pub status: TransactionStatus,
}
