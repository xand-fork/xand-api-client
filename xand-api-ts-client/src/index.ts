import * as grpc from "grpc";
import path from "path";

function proto_def_path(): string {
    // Since the file is copied into 'dist/' it'll be one level above us in 'src'
    return path.resolve(__dirname, '..', 'xand-api.proto');
}

export {
    grpc,
    proto_def_path
};