#!/usr/bin/env bash

set -e

SCRIPT_DIR="$(dirname ${BASH_SOURCE[0]})"

function fail() {
    echo "ERROR: $1"
    exit 1
}

function update_dependent_cargo() {
    local cargo_file="$1"
    local dependency_name="$2"
    local dependency_version="$3"
    local toml_query="$4"

    local version=$(toml get "$cargo_file" "$toml_query")
    if [[ "$version" == "null" ]]; then
	# Skipping dependency version bump for '${dependency_name}' in '${cargo_file}' (no existing version found)
	return
    fi    
    local new_cargo=$(toml set "$cargo_file" "$toml_query" "$dependency_version")
    echo "$new_cargo" > "$cargo_file"
}


function set_version_in_dependencies() {
    local cargo_file="$1"
    local dependency_name="$2"
    local dependency_version="$3"

    update_dependent_cargo "$cargo_file" "$dependency_name" "$dependency_version" "dependencies.${dependency_name}.version"
    update_dependent_cargo "$cargo_file" "$dependency_name" "$dependency_version" "dev-dependencies.${dependency_name}.version"
}

function set_version() {
    local cargo_file="$1"
    local version="$2"
    
    local new_cargo=$(toml set "$cargo_file" package.version "$version")
    echo "$new_cargo" > "$cargo_file"
}

function get_name() {
    local cargo_file="$1"

    local name=$(toml get "$cargo_file" package.name)
    if [[ "$crate_name" == "null" ]]; then
	fail "missing package name: ${cargo_file}"
    else
	echo "$name"
    fi
}

function main() {
    local cargo_file="$1"
    local new_version="$2"
    local base_dir="$(dirname ${SCRIPT_DIR})"

    which toml >/dev/null || fail "please install toml-cli"

    local crate_name=$(get_name "$cargo_file")
    set_version "$cargo_file" "$new_version"

    for dep_cargo_file in $(find "$base_dir" -name "Cargo.toml" -type f); do
    	set_version_in_dependencies "$dep_cargo_file" "$crate_name" "$new_version"
    done
}

main "$1" "$2"
