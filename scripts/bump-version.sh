#!/usr/bin/env bash

set -eu
set -o pipefail

SCRIPT_DIR="$(dirname ${BASH_SOURCE[0]})"

function fail() {
    echo "ERROR: $1"
    return 1
}

function bump() {
    local -r bump_type="$1"
    local -r version="$2"

    if ! [[ "$version" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        echo "null"
        exit 1
    fi

    local major=$(echo "$version" | cut -d'.' -f1)
    local minor=$(echo "$version" | cut -d'.' -f2)
    local patch=$(echo "$version" | cut -d'.' -f3)

    case "$bump_type" in
	major)
	    major=$(( $major + 1 ))
	    minor="0"
	    patch="0"
	    ;;
	minor)
	    minor=$(( $minor + 1 ))
	    patch="0"
	    ;;
	patch)
	    patch=$(( $patch + 1 ))
	    ;;
    esac

    echo "${major}.${minor}.${patch}"
}

function set_crate_version() {
    local -r crate_dir="${SCRIPT_DIR}/../$1"
    local -r bumped_version="$2"
    "${SCRIPT_DIR}/set-crate-version.sh" "${crate_dir}/Cargo.toml" "$bumped_version"
}

function set_npm_package_version() {
    local -r manifest_file="${SCRIPT_DIR}/../$1/package.json"
    local -r bumped_version="$2"
    local -r new_manifest="$(cat "$manifest_file" | jq ".version = \"${bumped_version}\"")"
    echo "$new_manifest" > "$manifest_file"
}

function main() {
    local -r bump_type="$1"

    if ! ${SCRIPT_DIR}/check-versions.sh; then
        exit 1
    fi

    local -r version="$(toml get "./xand-api-proto/Cargo.toml" package.version | sed 's/\"//g')"
    if [[ $version == "null" ]]; then
       fail "missing package version: ${cargo_file}"
    fi

    local bumped_version
    local bumped_version="$(bump "$bump_type" "$version")"
    if [[ $bumped_version == "null" ]]; then
	    fail "unsupported version string '$version'"
    fi

    set_crate_version ./xand-api-proto "$bumped_version"
    set_crate_version ./xand-api-client "$bumped_version"

    set_npm_package_version ./xand-api-ts-client "$bumped_version"
}

main "$1"
