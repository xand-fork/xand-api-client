#!/usr/bin/env bash

SCRIPT_DIR="$(dirname ${BASH_SOURCE[0]})"

pushd xand-api-ts-client
yarn install
yarn build
npm publish --tag latest
popd
